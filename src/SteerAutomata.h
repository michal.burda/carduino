/*
 * File name: SteerAutomata.h
 * Date:      2016/12/28 18:50
 * Author:    
 */

#ifndef __STEERAUTOMATA_H__
#define __STEERAUTOMATA_H__

#include "Automata.h"

/*
 * uplne vlevo na 2s = varovne svetla
 * uplne vpravo na 2s = hlavni svetla
 * uplne vlevo (vpravo), zpatky do poloviny, uplne vlevo (vpravo) = blinkr vlevo (vpravo)
 */

class SteerAutomata : public Automata<10,7> {
public:
    static const unsigned int S_READY = 0;
    static const unsigned int S_RUNNING = 1;
    static const unsigned int S_LEFT_A = 2;
    static const unsigned int S_LEFT_B = 3;
    static const unsigned int S_MAIN_LIGHTS = 4;
    static const unsigned int S_TURN_LEFT = 5;
    static const unsigned int S_RIGHT_A = 6;
    static const unsigned int S_RIGHT_B = 7;
    static const unsigned int S_WARN_LIGHTS = 8;
    static const unsigned int S_TURN_RIGHT = 9;

    static const unsigned int E_LEFT2 = 0;
    static const unsigned int E_LEFT1 = 1;
    static const unsigned int E_CENTER = 2;
    static const unsigned int E_RIGHT1 = 3;
    static const unsigned int E_RIGHT2 = 4;
    static const unsigned int E_RUN = 5;
    static const unsigned int E_IDLE = 6;

    SteerAutomata(AutomataListener &listener, unsigned int id = 0) : 
        Automata(listener, id)
    { }

    virtual void setup()
    { 
        setTransition(E_RUN, S_RUNNING);
        setTransition(S_RUNNING, E_IDLE, S_READY);

        for (unsigned int s = 2; s <= 9; ++s) {
            for (unsigned int e = 0; e <= 4; ++e) {
                setTransition(s, e, S_READY);
            }
        }

        setTransition(S_READY, E_LEFT2, S_LEFT_A);
        setTransition(S_LEFT_A, E_LEFT2, S_LEFT_A);
        setTransition(S_LEFT_A, 2000UL, S_MAIN_LIGHTS);
        setTransition(S_MAIN_LIGHTS, E_LEFT2, S_MAIN_LIGHTS);
        setTransition(S_LEFT_A, E_LEFT1, S_LEFT_B);
        setTransition(S_LEFT_B, E_LEFT1, S_LEFT_B);
        setTransition(S_LEFT_B, 1000UL, S_READY);
        setTransition(S_LEFT_B, E_LEFT2, S_TURN_LEFT);
        setTransition(S_TURN_LEFT, E_LEFT2, S_TURN_LEFT);

        setTransition(S_READY, E_RIGHT2, S_RIGHT_A);
        setTransition(S_RIGHT_A, E_RIGHT2, S_RIGHT_A);
        setTransition(S_RIGHT_A, 2000UL, S_WARN_LIGHTS);
        setTransition(S_WARN_LIGHTS, E_RIGHT2, S_WARN_LIGHTS);
        setTransition(S_RIGHT_A, E_RIGHT1, S_RIGHT_B);
        setTransition(S_RIGHT_B, E_RIGHT1, S_RIGHT_B);
        setTransition(S_RIGHT_B, 1000UL, S_READY);
        setTransition(S_RIGHT_B, E_RIGHT2, S_TURN_RIGHT);
        setTransition(S_TURN_RIGHT, E_RIGHT2, S_TURN_RIGHT);
    }

    void handleSteerSignal(int crisp)
    { handleEvent(crisp + 2); }

    void handleThrottleSignal(int crisp)
    { 
        if (crisp == 0) {
            handleEvent(E_IDLE);
        } else {
            handleEvent(E_RUN);
        }
    }
};

#endif
