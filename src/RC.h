/*
 * File name: RC.h
 * Date:      2016/12/03 19:01
 * Author:    
 */

#ifndef __RC_H__
#define __RC_H__

#include <Arduino.h>
#include "Component.h"
#include "Buffer.h"


template <unsigned int CHANNELS>
class RC : public Component {
private:
    static const int CALIBRATION_COUNT = 100;
    static const int BUFFER_SIZE = 3;

    unsigned int _addr[CHANNELS];
    
    Buffer<BUFFER_SIZE> _raw[CHANNELS];
    int _crisp[CHANNELS];
    float _rel[CHANNELS];
    float _zero[CHANNELS];
    unsigned int _zeroCount;
    unsigned int _relMax;

    void raw2rel(const int i) {
        float value = (1.0 * _raw[i].get() - _zero[i]);
        if (value > 1.0 * _relMax) {
            value = 1.0;
        } else if (value < -1.0 * _relMax) {
            value = -1.0;
        //} else if (abs(value) < 0.07 * _relMax) {
            //value = 0.0;
        } else {
            value = 1.0 * value / _relMax;
        }
        _rel[i] = value;
    }

    void raw2crisp(const int i) {
        int diff = _raw[i].get() - _zero[i];
        int dist = abs(diff);
        if (dist < 0.1 * _relMax) {
            _crisp[i] = 0;
        } else {
            int sign = diff < 0 ? -1 : 1;
            if (dist > 0.9 * _relMax) {
                _crisp[i] = 2 * sign;
            } else {
                _crisp[i] = sign;
            }
        }
    }

    void rawRead(unsigned int i)
    {
        int r = pulseIn(_addr[i], HIGH);
        //if (r > 100) {
            _raw[i].put(r);
        //}
    }


    void read()
    {
    }

public:
    RC(const unsigned int* addr, const unsigned int relMax) : 
        _zeroCount(0),
        _relMax(relMax)
    {
        for (int i = 0; i < CHANNELS; ++i) {
            for (int j = 0; j < BUFFER_SIZE; ++j) {
                _raw[i].put(1450);
            }
            _zero[i] = 0;
            _addr[i] = addr[i];
        }
    }

    int getRaw(const int i) const
    { return _raw[i].get(); }

    int getCrisp(const int i) const
    { return _crisp[i]; }

    float getRel(const int i) const
    { return _rel[i]; }

    float getZero(const int i) const
    { return _zero[i]; }

    bool isCalibrated() const
    { return _zeroCount >= CALIBRATION_COUNT; }

    virtual void setup()
    {
        for (int i = 0; i < CHANNELS; ++i) {
            pinMode(_addr[i], INPUT);
        }

        while (_zeroCount < CALIBRATION_COUNT) {
            for (int i = 0; i < CHANNELS; ++i) {
                //_raw[i] = pulseIn(_addr[i], HIGH);
                rawRead(i);
                _zero[i] = (_zero[i] * _zeroCount + _raw[i].get()) / (_zeroCount + 1);
            }
            _zeroCount++;
        }
    }

    virtual void loop()
    {
        for (int i = 0; i < CHANNELS; ++i) {
            //_raw[i] = pulseIn(_addr[i], HIGH);
            rawRead(i);
            raw2rel(i);
            raw2crisp(i);
        }
    }
};

#endif
