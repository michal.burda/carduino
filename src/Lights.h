/*
 * File name: Lights.h
 * Date:      2016/12/03 17:31
 * Author:    
 */

#ifndef __LIGHTS_H__
#define __LIGHTS_H__

#include <Arduino.h>
#include "Component.h"


template <unsigned int COUNT>
class Lights : public Component {
private:
    unsigned int _addr[COUNT];

public:
    Lights(const unsigned int* addr)
    { 
        for (int i = 0; i < COUNT; ++i) {
            _addr[i] = addr[i];
        }
    }

    virtual void setup()
    {
        for (int i = 0; i < COUNT; ++i) {
            pinMode(_addr[i], OUTPUT);
        }
    }

    void on(unsigned long mask = 0xFFFFFFFF, byte intensity = 255) 
    {
        for (int i = 0; i < COUNT; ++i) {
            if ((mask >> i) & 1) {
                analogWrite(_addr[i], intensity);
            }
        }
    }

    void off()
    {
        for (int i = 0; i < COUNT; ++i) {
            digitalWrite(_addr[i], LOW);
        }
    }

};

#endif
