/*
 * File name: Automata.h
 * Date:      2016/12/03 16:19
 * Author:    
 */

#ifndef __AUTOMATA_H__
#define __AUTOMATA_H__

#include <Arduino.h>
#include "Component.h"


class AutomataListener {
public:
    virtual void onStateChange(unsigned int id, unsigned int from, unsigned int to) = 0;
};


template <unsigned int STATES, unsigned int EVENTS>
class Automata : public Component {
private:
    unsigned int _current;
    unsigned long _time;
    unsigned int _transitions[STATES][EVENTS+1]; // EVENTS-th event is the timeout event
    unsigned long _timeouts[STATES];
    AutomataListener &_listener;
    unsigned int _id; 

public:
    Automata(AutomataListener &listener, unsigned int id = 0) : 
        _current(0), _time(millis()), _listener(listener), _id(id)
    { 
        for (unsigned int s = 0; s < STATES; ++s) {
            _timeouts[s] = 0;
            for (unsigned int e = 0; e <= EVENTS; ++e) { 
                _transitions[s][e] = s;
            }
        }
    }

    unsigned int getId()
    { return _id; }

    //void setListener(Listener &listener)
    //{ _listener = listener; }

    void setTransition(unsigned int event, unsigned int to)
    {
        for (unsigned int s = 0; s < STATES; ++s) {
            _transitions[s][event] = to;
        }
    }

    void setTransition(unsigned int from, unsigned int event, unsigned int to)
    { 
        _transitions[from][event] = to;
    }

    void setTransition(unsigned int from, unsigned long timeout, unsigned int to)
    {
        _timeouts[from] = timeout;
        _transitions[from][EVENTS] = to;
    }

    void handleEvent(unsigned int event) {
        unsigned int newState = _transitions[_current][event];
        if (_current != newState) {
            _listener.onStateChange(_id, _current, newState);
            _current = newState;
            _time = millis();
        }
    }

    virtual void setup()
    { }

    virtual void loop() {
        if (_timeouts[_current] > 0 && _timeouts[_current] + _time < millis()) {
            handleEvent(EVENTS);
        }
    }
};


#endif
