/*
 * File name: Buffer.h
 * Date:      2016/12/27 11:37
 * Author:    
 */

#ifndef __BUFFER_H__
#define __BUFFER_H__


template <unsigned int SIZE>
class Buffer {
private:
    unsigned int _vals[SIZE];
    unsigned int _index;
    unsigned int _sum;

public:
    Buffer() : _index(0), _sum(0)
    { 
        for (unsigned int i = 0; i < SIZE; ++i) {
            _vals[i] = 0;
        }
    }

    void put(unsigned int value)
    {
        _sum = _sum + value - _vals[_index];
        _vals[_index] = value;
        _index = (_index + 1) % SIZE;
    }

    unsigned int get()
    { return _sum / SIZE; }
};

#endif
