/*
 * File name: BlinkingLights.h
 * Date:      2016/12/03 17:29
 * Author:    
 */

#ifndef __BLINKINGLIGHTS_H__
#define __BLINKINGLIGHTS_H__

#include "Container.h"
#include "Lights.h"
#include "Automata.h"


template <unsigned int COUNT>
class BlinkingLights : public Container, public AutomataListener {
private:
    const unsigned int S_OFF = 0;
    const unsigned int S_BLINK = 1;
    const unsigned int S_PAUSE = 2;
    const unsigned int S_ON = 3;

    const unsigned int E_BLINK = 0;
    const unsigned int E_OFF = 1;
    const unsigned int E_ON = 2;

    Lights<COUNT> _lights;
    unsigned long _mask;
    Automata<4, 3> _automata;
    unsigned long _onDelay;
    unsigned long _offDelay;

public:
    BlinkingLights(const unsigned int* addr,
            const unsigned long onDelay = 1000UL,
            const unsigned long offDelay = 500UL) : 
        _lights(addr), 
        _mask(0UL),
        _automata(*this),
        _onDelay(onDelay),
        _offDelay(offDelay)
    { 
        append(&_lights);
        append(&_automata);
    }

    virtual void setup()
    { 
        Container::setup();
        _automata.setTransition(E_OFF, S_OFF);
        _automata.setTransition(E_BLINK, S_BLINK);
        _automata.setTransition(E_ON, S_ON);
        _automata.setTransition(S_BLINK, _onDelay, S_PAUSE);
        _automata.setTransition(S_PAUSE, _offDelay, S_BLINK);
    }

    virtual void onStateChange(unsigned int id, unsigned int from, unsigned int to)
    {
        if (to == S_ON || to == S_BLINK) {
            _lights.on(_mask);
        } else if (from == S_ON || from == S_BLINK) {
            _lights.off();
        }
    }

    void on(unsigned long mask = 0xFFFFFFFF) 
    {
        _mask = mask;
        _automata.handleEvent(E_ON);
    }

    void off()
    {
        _automata.handleEvent(E_OFF);
    }

    void blink(unsigned long mask = 0xFFFFFFFF) 
    {
        _mask = mask;
        _automata.handleEvent(E_BLINK);
    }
};

#endif
