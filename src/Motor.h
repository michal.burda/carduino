/*
 * File name: Motor.h
 * Date:      2016/12/26 10:38
 * Author:    
 */

#ifndef __MOTOR_H__
#define __MOTOR_H__

#include <Arduino.h>
#include "Component.h"


class Motor : public Component {
private:
    unsigned int _addr;
    Servo _servo;
    int _value;
    int _neutral;
    int _dead;
    int _relMax;

public:
    Motor(const unsigned int addr, const unsigned int neutral,
            const unsigned int dead, const unsigned int relMax) :
        _addr(addr), _value(0), _neutral(neutral), _dead(dead), _relMax(relMax)
    { }

    void setValue(const float v)
    {
        if (abs(v * _relMax) > _dead) {
            _value = _neutral + (unsigned int) (v * _relMax);
        } else {
            _value = _neutral;
        }
        //Serial.print(v);
        //Serial.print("...");
        //Serial.print(_value);
        //Serial.print("\n");
    }

    virtual void setup()
    { _servo.attach(_addr); }

    virtual void loop()
    { 
        _servo.write(_value); 
    }
};

#endif
