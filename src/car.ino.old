#include <Servo.h>
#include "Automata.h"
#include "Scheduler.h"
#include "Lights.h"
#include "BlinkingLights.h"

const int steer = 0;
const int throttle = 1;
const int channelCount = 2;

const int ledLRev = 2;
const int ledRRev = 3;
const int ledLRear = 5;
const int ledRRear = 6;
const int ledLFront = 4;
const int ledRFront = 7;

const int rearLightIntensity = 16;

const int ledLFBlink = 0;
const int ledRFBlink = 1;
const int ledLRBlink = 2;
const int ledRRBlink = 3;
const int blinkAddr[4] = {19, 18, 17, 16};

bool blinking[4] = {false, false, false, false};

bool mainLightsState;
bool blinkState;


const int servoAddr[channelCount] = {9, 10};
const int rcAddr[channelCount] = {11, 12};

const int deadVar[channelCount] = {50, 0};
const int maxVar[channelCount] = {330, 330};
const int shift[channelCount] = {0, 150};

int raw[channelCount] = {0, 0};
int rel[channelCount] = {0, 0};
int oldRel[channelCount] = {0, 0};
int neutral[channelCount] = {0, 0};

Servo servo[channelCount];

unsigned long startMillis = 0;
unsigned long calibrationCount = 0;
unsigned long lastBlinkChangeMillis = 0;
unsigned long lastSteerMin = 0;
unsigned long lastSteerMax = 0;

const unsigned long blinkDelay = 1000;
const unsigned long doubleClickDelay = 1000;

const int FWD = 0;
const int NON = 1;
const int BRK = 2;
const int REV = 3;

int state = FWD;
int oldState = FWD;
int stateTransition[4][5] {
    // -2   -1    0    1    2    = rel
    { BRK, BRK, FWD, FWD, FWD }, // FWD (forward)
    { REV, REV, NON, FWD, FWD }, // NON (neutral)
    { BRK, BRK, NON, FWD, FWD }, // BRK (braking)
    { REV, REV, NON, FWD, FWD }  // REV (reverse)
};


void readRaw() {
  for (int i = 0; i < channelCount; i++) {
    raw[i] = pulseIn(rcAddr[i], HIGH);
  }
}


void updateNeutral() {
  for (int i = 0; i < channelCount; i++) {
    neutral[i] = (calibrationCount * neutral[i] + raw[i]) / (calibrationCount + 1);
  }
  calibrationCount++;
}


void raw2rel() {
  for (int i = 0; i < channelCount; i++) {
    if (raw[i] > neutral[i] + 0.9 * maxVar[i]) {
        rel[i] = -2;
    } else if (raw[i] > neutral[i] + 0.1 * maxVar[i]) {
        rel[i] = -1;
    } else if (raw[i] < neutral[i] - 0.9 * maxVar[i]) {
        rel[i] = 2;
    } else if (raw[i] < neutral[i] - 0.1 * maxVar[i]) {
        rel[i] = 1;
    } else {
        rel[i] = 0;
    }
  }
}


void rel2old() {
  for (int i = 0; i < channelCount; i++) {
    oldRel[i] = rel[i];
  }
}

int remap(int value, int neutral, int maxVar, int deadVar, int shift) {
  int lo = neutral - maxVar;
  int hi = neutral + maxVar;
  int deadLo = neutral - deadVar;
  int deadHi = neutral + deadVar;
  if (value < lo) {
    value = lo;
  } else if (value > hi) {
    value = hi;
  } else if (value >= deadLo && value <= deadHi) {
    value = (deadLo + deadHi) / 2;
  }
  return value + shift;
}


void raw2servo() {
  for (int i = 0; i < channelCount; i++) {
    int val = remap(raw[i], neutral[i], maxVar[i], deadVar[i], shift[i]);
    servo[i].write(val);
  }
}


void forwardState() {
    switch (rel[throttle]) {
        case 0:
            state = NON;
            break;
    }
}


void neutralState() {
}


void brakeState() {
}


void reverseState() {
}


void relThrottleChanged(int from, int to) {
    switch (from) {
        case -2:
        case -1:
            break;
        case 0:
            break;
        case 1:
        case 2:
            break;
    }

    switch (to) {
        case -2:
        case -1:
            break;
        case 0:
            break;
        case 1:
        case 2:
            break;
    }
}


void relSteerChanged(int from, int to) {
    switch (from) {
        case -2:
        case -1:
            break;
        case 0:
            break;
        case 1:
        case 2:
            break;
    }

    switch (to) {
        case -2:
            if (lastSteerMin >= millis() - doubleClickDelay) {
                leftBlink(true);
            } else {
                lastSteerMin = millis();
            }
            break;
        case -1:
            rightBlink(false);
            break;
        case 0:
            break;
        case 1:
            leftBlink(false);
            break;
        case 2:
            if (lastSteerMax >= millis() - doubleClickDelay) {
                rightBlink(true);
            } else {
                lastSteerMax = millis();
            }
            break;
    }
}


void mainLights(bool on = mainLightsState) {
    mainLightsState = on;
    if (on) {
        analogWrite(ledLFront, 255);
        analogWrite(ledRFront, 255);
        analogWrite(ledLRear, rearLightIntensity);
        analogWrite(ledRRear, rearLightIntensity);
    } else {
        digitalWrite(ledLFront, LOW);
        digitalWrite(ledRFront, LOW);
        analogWrite(ledLRear, 0);
        analogWrite(ledRRear, 0);
    }
}


void forceBlinkingCycleChange(bool on) {
    lastBlinkChangeMillis = millis() - blinkDelay;
    blinkState = on;
}


void leftBlink(bool on) {
    blinking[ledLFBlink] = on;
    blinking[ledLRBlink] = on;
    forceBlinkingCycleChange(on);
}


void rightBlink(bool on) {
    blinking[ledRFBlink] = on;
    blinking[ledRRBlink] = on;
    forceBlinkingCycleChange(on);
}


void updateBlinks() {
    if (lastBlinkChangeMillis < millis() - blinkDelay) {
        lastBlinkChangeMillis = millis();
        blinkState = !blinkState;
        for (int i = 0; i < 4; i++) {
            int what = (blinking[i] & blinkState) ? HIGH : LOW;
            digitalWrite(blinkAddr[i], what);
        }
    }
}


void brakeLights(bool on) {
    if (on) {
        analogWrite(ledLRear, 255);
        analogWrite(ledRRear, 255);
    } else {
        mainLights();
    }
}


void reverseLights(bool on) {
    int what = on ? HIGH : LOW;
    digitalWrite(ledLRev, what);
    digitalWrite(ledRRev, what);
}



void stateChanged(int from, int to) {
    switch (from) {
        case FWD:
            Serial.print("FWD");
            break;
        case NON:
            Serial.print("NON");
            break;
        case BRK:
            Serial.print("BRK");
            brakeLights(false);
            break;
        case REV:
            Serial.print("REV");
            reverseLights(false);
            break;
    }

    Serial.print("->");

    switch (to) {
        case FWD:
            Serial.println("FWD");
            break;
        case NON:
            Serial.println("NON");
            break;
        case BRK:
            Serial.println("BRK");
            brakeLights(true);
            break;
        case REV:
            Serial.println("REV");
            reverseLights(true);
            break;
    }
}


void setup() {
  Serial.begin(9600);
  Serial.println("Starting");
  for (int i = 0; i < channelCount; i++) {
    pinMode(rcAddr[i], INPUT);
    servo[i].attach(servoAddr[i]);
  }
  for (int i = 0; i < 4; i++) {
    pinMode(blinkAddr[i], OUTPUT);
  }
  pinMode(ledLRev, OUTPUT);
  pinMode(ledRRev, OUTPUT);
  pinMode(ledLRear, OUTPUT);
  pinMode(ledRRear, OUTPUT);
  startMillis = millis();
  mainLights(true);
}


void loop() {
  readRaw();

  if (millis() - startMillis < 1000) {    
    updateNeutral();

  } else {
    updateBlinks();
    raw2servo();
    rel2old();
    raw2rel();

    if (oldRel[throttle] != rel[throttle]) {
        relThrottleChanged(oldRel[throttle], rel[throttle]);
    }
    if (oldRel[steer] != rel[steer]) {
        relSteerChanged(oldRel[steer], rel[steer]);
    }

    oldState = state;
    state = stateTransition[state][rel[throttle] + 2];

    if (oldState != state) {
        stateChanged(oldState, state);
    }
  }
}
