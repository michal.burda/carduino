/*
 * File name: LightingSystem.h
 * Date:      2016/12/28 10:20
 * Author:    
 */

#ifndef __LIGHTINGSYSTEM_H__
#define __LIGHTINGSYSTEM_H__

#include "Container.h"
#include "Lights.h"
#include "BlinkingLights.h"
#include "Automata.h"


class LightingSystem : public Container {
private:
    Lights<2> _frontLights;
    Lights<2> _rearLights;
    Lights<2> _reverseLights;
    BlinkingLights<4> _turnLights;
    bool _mainLightsOn;
    bool _brakeLightsOn;
    bool _reverseLightsOn;
    bool _warningLightsOn;
    int _turnLightsOn;

public:
    LightingSystem() :
        _frontLights((const unsigned int[]) {A4, A5}),
        _rearLights((const unsigned int[]) {5, 6}),
        _reverseLights((const unsigned int[]) {4, 8}),
        _turnLights((const unsigned int[]) {2, 13, 3, 7}),
        _mainLightsOn(false),
        _brakeLightsOn(false),
        _reverseLightsOn(false),
        _warningLightsOn(false),
        _turnLightsOn(0)
    { 
        append(&_frontLights);
        append(&_rearLights);
        append(&_reverseLights);
        append(&_turnLights);
    }

    void mainLights(bool on)
    {
        _mainLightsOn = on;
        if (on) {
            _frontLights.on();
                _rearLights.on(0xFFFFFFFF, 55);
            //if (!_brakeLightsOn) {
                //_rearLights.on(0xFFFFFFFF, 15);
            //}
        } else {
            _frontLights.off();
            if (!_brakeLightsOn) {
                _rearLights.off();
            }
        }
    }

    void toggleMainLights()
    {
        mainLights(!_mainLightsOn);
    }

    void brakeLights(bool on)
    {
        _brakeLightsOn = on;
        if (on) {
            _rearLights.on(0xFFFFFFFF, 255);
        } else if (_mainLightsOn) {
            _rearLights.on(0xFFFFFFFF, 15);
        } else {
            _rearLights.off();
        }
    }

    void reverseLights(bool on)
    {
        _reverseLightsOn = on;
        if (on) {
            _reverseLights.on();
        } else {
            _reverseLights.off();
        }
    }

    void turnLeftLights()
    { 
        _turnLightsOn = -1;
        if (!_warningLightsOn) {
            _turnLights.blink(3);
        }
    }

    void turnRightLights()
    {
        _turnLightsOn = 1;
        if (!_warningLightsOn) {
            _turnLights.blink(12);
        }
    }

    void turnLightsOff()
    {
        _turnLightsOn = 0;
        if (!_warningLightsOn) {
            _turnLights.off();
        }
    }

    void warningLights(bool on)
    {
        _warningLightsOn = on;
        if (on) {
            _turnLights.blink();
        } else {
            if (_turnLightsOn < 0) {
                _turnLights.blink(3);
            } else if (_turnLightsOn > 0) {
                _turnLights.blink(12);
            } else {
                _turnLights.off();
            }
        }
    }

    void toggleWarningLights()
    { warningLights(!_warningLightsOn); }
};

#endif
