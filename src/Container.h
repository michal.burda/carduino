/*
 * File name: Container.h
 * Date:      2016/12/04 14:39
 * Author:    
 */

#ifndef __CONTAINER_H__
#define __CONTAINER_H__

#include <Arduino.h>
#include "Vector.h"
#include "Component.h"


class Container : public Component {
private:
    Vector<Component*> _components;

public:
    Container() : _components()
    { }

    void append(Component *component)
    { _components.push_back(component); }

    virtual void setup()
    {
        for (size_t i = 0; i < _components.size(); ++i) {
            _components[i]->setup();
        }
    }

    virtual void loop()
    {
        for (size_t i = 0; i < _components.size(); ++i) {
            _components[i]->loop();
        }
    }
};

#endif
