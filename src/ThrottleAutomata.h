/*
 * File name: ThrottleAutomata.h
 * Date:      2016/12/28 18:42
 * Author:    
 */

#ifndef __THROTTLEAUTOMATA_H__
#define __THROTTLEAUTOMATA_H__

#include "Automata.h"


class ThrottleAutomata : public Automata<4,3> {
public:
    static const unsigned int S_FORWARD = 0;
    static const unsigned int S_IDLE = 1;
    static const unsigned int S_BRAKING = 2;
    static const unsigned int S_REVERSE = 3;

    static const unsigned int E_FORWARD = 0;
    static const unsigned int E_IDLE = 1;
    static const unsigned int E_REVERSE = 2;

    ThrottleAutomata(AutomataListener &listener, unsigned int id = 0) : 
        Automata(listener, id)
    { }

    virtual void setup()
    { 
        setTransition(E_FORWARD, S_FORWARD);
        setTransition(S_FORWARD, E_IDLE, S_FORWARD);
        setTransition(S_FORWARD, E_REVERSE, S_BRAKING);
        setTransition(S_IDLE, E_REVERSE, S_REVERSE);
        setTransition(S_BRAKING, E_IDLE, S_IDLE);
        setTransition(S_REVERSE, E_IDLE, S_IDLE);

    }

    void handleThrottleSignal(int crisp)
    {
        if (crisp < 0) {
            handleEvent(E_FORWARD);
        } else if (crisp > 0) {
            handleEvent(E_REVERSE);
        } else {
            handleEvent(E_IDLE);
        }
    }
};

#endif
