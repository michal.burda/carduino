/*
 * File name: Vector.h
 * Date:      2016/12/04 14:43
 * Author:    
 */

#ifndef __VECTOR_H__
#define __VECTOR_H__


template<typename DATA>
class Vector {
private:
    size_t _size;
    size_t _capacity;
    DATA *_data;

    void resize() { 
        _capacity = _capacity ? _capacity * 2 : 1;
        DATA *newdata = (DATA *) malloc(_capacity * sizeof(DATA));
        memcpy(newdata, _data, _size * sizeof(DATA));
        free(_data);
        _data = newdata;
    }

public:
    Vector() :
        _size(0), _capacity(0), _data(0)
    {}

    Vector(Vector const &other) :
        _size(other._size), _capacity(other._capacity), _data(0)
    {
        _data = (DATA *) malloc(_capacity * sizeof(DATA));
        memcpy(_data, other._data, _size * sizeof(DATA));
    }

    ~Vector()
    { free(_data); }

    Vector &operator=(Vector const &other)
    {
        free(_data);
        _size = other._size;
        _capacity = other._capacity;
        _data = (DATA *) malloc(_capacity * sizeof(DATA));
        memcpy(_data, other._data, _size * sizeof(DATA));
        return *this;
    }

    void push_back(DATA const &x) 
    {
        if (_capacity == _size)
            resize();
        _data[_size++] = x;
    }

    size_t size() const
    { return _size; }

    DATA const &operator[](size_t idx) const
    { return _data[idx]; }

    DATA &operator[](size_t idx)
    { return _data[idx]; }
};

#endif
