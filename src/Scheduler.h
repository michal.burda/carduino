/*
 * File name: Scheduler.h
 * Date:      2016/11/27 15:48
 * Author:    
 */

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__


class Scheduler;


class Task {
public:
    typedef void (*TaskFunc)(Scheduler *scheduler);

private:
    unsigned long time;
    int event;
    TaskFunc func;

public:
    Task() : time(0), event(0), func(0)
    { }

    Task(unsigned long t, TaskFunc f) :
        time(t), event(0), func(f)
    { }

    Task(unsigned long t, int e, TaskFunc f) :
        time(t), event(e), func(f)
    { }

    void erase()
    { time = 0; event = 0; func = 0; }

    bool isEmpty()
    { return time == 0; }

    bool expires(unsigned long t)
    { return time < t; }

    bool observes(int e)
    { return event == e; }

    void fire(Scheduler *scheduler)
    { 
        if (func) {
            func(scheduler);
        }
    }
};


class Scheduler {
private:
    static const unsigned int poolSize = 10;
    Task pool[poolSize];
    
public:
    void processTime(unsigned long currentTime) {
        for (unsigned int i = 0; i < poolSize; i++) {
            Task &t = pool[i];
            if (!t.isEmpty()) {
                if (t.expires(currentTime)) {
                    if (t.observes(0)) {
                        t.fire(this);
                    }
                    t.erase();
                }
            }
        }
    }

    void processEvent(int event) {
        for (unsigned int i = 0; i < poolSize; i++) {
            Task &t = pool[i];
            if (!t.isEmpty()) {
                if (t.observes(event)) {
                    t.fire(this);
                    t.erase();
                }
            }
        }
    }

    void schedule(Task &task) {
        for (unsigned int i = 0; i < poolSize; i++) {
            if (pool[i].isEmpty()) {
                pool[i] = task;
                break;
            }
        }
    }
};


#endif
