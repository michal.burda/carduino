/*
 * File name: Component.h
 * Date:      2016/12/04 14:36
 * Author:    
 */

#ifndef __COMPONENT_H__
#define __COMPONENT_H__

#include <Arduino.h>


class Component {
public:
    virtual void setup()
    { }

    virtual void loop()
    { }
};

#endif
