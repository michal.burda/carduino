#include <Arduino.h>
#include <Servo.h>
#include "Container.h"
#include "Motor.h"
#include "RC.h"
#include "LightingSystem.h"
#include "ThrottleAutomata.h"
#include "SteerAutomata.h"


/*
    Input PINs:
    1: motor (black) - ground
    2: motor (red)   - VIN
    3: motor (white) - PWM
    4: servo (brown) - ground
    5: servo (red)   - VIN
    6: servo (orange)- PWM
    7: RC (right)    - ground
    8: RC (middle)   - VIN

    Arduino GPIO:
    A0:
    A1:
    A2:
    A3:
    A4:  front (left)
    A5:  front (right)
    A6:  
    A7:
    D2:  rear blinking (right)
    D3:  rear blinking (left)
    D4:  reverse (right)
old D5:  reverse (left)
new D5:  rear red (left)
old D6:  front blinking (right)
new D6:  rear red (right)
    D7:  front blinking (left)
old D8:  rear red (left)
new D8:  reverse (left)
    D9:  servo PWM
    D10: motor PWM
    D11: RC channel 1
    D12: RC channel 2
old D13:  rear red (right)
new D13:  front blinking (right)
*/

class Car : public Container, public AutomataListener {
private:
    RC<2> _rc;
    LightingSystem _lightingSystem;
    ThrottleAutomata _throttleAutomata;
    SteerAutomata _steerAutomata;
    Motor _steerMotor;
    Motor _throttleMotor;

public:
    Car() :
        _rc((const unsigned int[]) {11, 12}, 300),
        _steerMotor(9, 1250, 30, 250),
        _throttleMotor(10, 1500, 15, 300),
        _throttleAutomata(*this, 0),
        _steerAutomata(*this, 1)
    {
        append(&_rc);
        append(&_lightingSystem);
        append(&_steerMotor);
        append(&_throttleMotor);
        append(&_throttleAutomata);
        append(&_steerAutomata);
    }

    virtual void onStateChange(const unsigned int id, const unsigned int from, const unsigned int to)
    {
        if (id == 0) { // throttle automata
            if (from == ThrottleAutomata::S_BRAKING) {
                _lightingSystem.brakeLights(false);
            } else if (from == ThrottleAutomata::S_REVERSE) {
                _lightingSystem.reverseLights(false);
            }
            if (to == ThrottleAutomata::S_BRAKING) {
                _lightingSystem.brakeLights(true);
            } else if (to == ThrottleAutomata::S_REVERSE) {
                _lightingSystem.reverseLights(true);
            }
        } else if (id == 1) { // steer automata
            if (to == SteerAutomata::S_MAIN_LIGHTS) {
                _lightingSystem.toggleMainLights();
            } else if (to == SteerAutomata::S_WARN_LIGHTS) {
                _lightingSystem.toggleWarningLights();
            } else if (to == SteerAutomata::S_TURN_LEFT) {
                _lightingSystem.turnLeftLights();
            } else if (to == SteerAutomata::S_LEFT_A) {
                _lightingSystem.turnLightsOff();
            } else if (to == SteerAutomata::S_TURN_RIGHT) {
                _lightingSystem.turnRightLights();
            } else if (to == SteerAutomata::S_RIGHT_A) {
                _lightingSystem.turnLightsOff();
            }
        }
    }

    virtual void setup()
    { 
        /*Serial.begin(9600); */
        Container::setup();

        for (int i = 0; i <= 3; ++i) {
            delay(50);
            _lightingSystem.mainLights(true);
            delay(100);
            _lightingSystem.mainLights(false);
        }
    }

    virtual void loop()
    { 
        /*digitalWrite(A3, LOW);*/
        /*delay(1000);*/
        /*digitalWrite(A3, HIGH);*/
        /*delay(1000);*/

        /*Serial.print(_rc.getRaw(0));*/
        /*Serial.print("...");*/
        /*Serial.print(_rc.getRel(0));*/
        /*Serial.print(" || ");*/
        /*Serial.print(_rc.getRaw(1));*/
        /*Serial.print("...");*/
        /*Serial.print(_rc.getRel(1));*/
        /*Serial.print("\n");*/

        _steerMotor.setValue(_rc.getRel(0));
        _steerAutomata.handleSteerSignal(_rc.getCrisp(0));
        _steerAutomata.handleThrottleSignal(_rc.getCrisp(1));

        _throttleMotor.setValue(_rc.getRel(1));
        _throttleAutomata.handleThrottleSignal(_rc.getCrisp(1));

        Container::loop();
    }
};





Car c;

void setup() {
    c.setup();
}

void loop() {
    c.loop();
}
